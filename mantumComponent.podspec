Pod::Spec.new do |spec|

  spec.name         = "MantumComponent"
  spec.version      = "0.1.2"
  spec.summary      = "A CocoaPods library written in Swift"

  spec.description  = "Help mantum tu use some interfaces, that helps with the structure of iOS apps."
  
  spec.homepage     = "https://bitbucket.org/3astronautas/mantum-component-iospodspecs"
  spec.license      = { :type => "MIT", :file => "LICENSE" }
  spec.author       = { "Mantum" => "juanes@tresastronautas.com" }

  spec.ios.deployment_target = "12.1"
  spec.swift_version = "4.2"
  spec.framework = "UIKit"

  spec.source        = { :git => "https://jechaparro@bitbucket.org/3astronautas/mantum-component-ios.git", :tag => "#{spec.version}" }
  spec.source_files  = "MantumComponent/**/*.{h,m,swift}"

end

