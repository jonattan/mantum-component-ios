//
//  DatePickerComponent.swift
//  MantumComponent
//
//  Created by Jonattan Velasquez on 26/07/18.
//  Copyright © 2018 Mantum. All rights reserved.
//

import UIKit

public class DatePickerComponent: Picker<UIDatePicker> {
    
    public init(controlador: MantumController) {
        let pickerView = UIDatePicker()
        pickerView.datePickerMode = UIDatePicker.Mode.date
        super.init(controlador: controlador, picker: pickerView)
    }
}
