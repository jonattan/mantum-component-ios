//
//  PickerComponent.swift
//  MantumComponent
//
//  Created by Jonattan Velasquez on 26/07/18.
//  Copyright © 2018 Mantum. All rights reserved.
//

import UIKit

public class PickerComponent: Picker<UIPickerView> {
    
    public init(controlador: MantumController, pickerViewDelegate: UIPickerViewDelegate) {
        let pickerView = UIPickerView()
        pickerView.delegate = pickerViewDelegate
        super.init(controlador: controlador, picker: pickerView)
    }
    
    public func selectRow(index : Int) -> Void {
        self.picker.selectRow(index, inComponent: 0, animated: false)
    }
}
