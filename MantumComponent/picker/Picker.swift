//
//  Picker.swift
//  MantumComponent
//
//  Created by Jonattan Velasquez on 26/07/18.
//  Copyright © 2018 Mantum. All rights reserved.
//

import Foundation

import UIKit

public class Picker<T> {
    
    let picker: T
    
    let controlador: MantumController
    
    public init(controlador: MantumController, picker: T) {
        self.controlador = controlador
        self.picker = picker
    }
    
    public func inputView() -> T {
        return self.picker
    }
    
    public func inputAccessoryView(titulo: String = "", selector: Selector? = nil) -> UIToolbar {
        let toolBar = UIToolbar(frame: CGRect(x: 0, y: self.controlador.view.frame.size.height / 6, width: self.controlador.view.frame.size.width, height: 40.0))
        toolBar.layer.position =
            CGPoint(x: self.controlador.view.frame.size.width/2, y: controlador.view.frame.size.height-20.0)
        toolBar.barStyle = UIBarStyle.blackTranslucent
        toolBar.tintColor = UIColor.white
        toolBar.backgroundColor = MantumController.hexStringToUIColor(hex: "F5F5F5")
        
        let doneButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.done,
                                         target: controlador, action: selector)
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace,
                                        target: controlador, action: nil)
        
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: self.controlador.view.frame.size.width / 3, height: self.controlador.view.frame.size.height))
        label.backgroundColor = UIColor.clear
        label.textColor = UIColor.white
        label.text = titulo
        label.textAlignment = NSTextAlignment.center
        
        let textBtn = UIBarButtonItem(customView: label)
        toolBar.setItems([flexSpace, textBtn, flexSpace, doneButton], animated: true)
        return toolBar
    }
}
