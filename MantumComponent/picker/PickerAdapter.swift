//
//  PickerAdapter.swift
//  MantumComponent
//
//  Created by Jonattan Velasquez on 26/07/18.
//  Copyright © 2018 Mantum. All rights reserved.
//

import Foundation

public class PickerAdapter<T> {
    
    public let id: T
    
    public let name: String
    
    public init(id: T, name: String) {
        self.id = id
        self.name = name
    }
}
