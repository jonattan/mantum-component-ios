//
//  GroupAdapter.swift
//  MantumComponent
//
//  Created by Jonattan Velásquez on 2/07/18.
//  Copyright © 2018 Mantum. All rights reserved.
//

import UIKit

public class GroupAdapter: NSObject, UITableViewDataSource, UITableViewDelegate {
    
    private var original = [(key: String, value: [String])]()
    
    private let onCellForRowAt: ((_ value: String, _ indexPath: IndexPath, _ tableView: UITableView) -> UITableViewCell)
    
    public var onViewForHeaderInSection: ((_ value: String, _ tableView: UITableView, _ section: Int) -> UIView)? = nil
    
    public func includeNewElements(value: [String]) -> Void {
        self.original.removeAll()
        
        value.forEach { item in
            let title = item
            if title.isEmpty {
                return
            }
            
            let letter = title
            let index = self.original.index(where: { (key, value) -> Bool in
                return key == letter
            })

            if index == nil {
                self.original.append((key: letter, value: [item]))
            } else {
                var include: Bool = true
                self.original[index!].value.forEach({ original in
                    if original.isEqual(item) {
                        include = false
                        return
                    }
                })
                
                if include {
                    self.original[index!].value.append(item)
                }
            }
        }
    }
    
    public init(onCellForRowAt: @escaping (String, IndexPath, UITableView) -> UITableViewCell) {
        self.onCellForRowAt = onCellForRowAt
        super.init()
    }
    
    public func numberOfSections(in tableView: UITableView) -> Int {
        return self.original.count
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.original[section].value.count
    }
    
    public func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return original[section].key
    }
    
    public func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if self.onViewForHeaderInSection != nil {
            return self.onViewForHeaderInSection!(self.original[section].key, tableView, section)
        }
        return nil
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return self.onCellForRowAt(self.getValue(indexPath: indexPath), indexPath, tableView)
    }
    
    public func isEmpty() -> Bool {
        return self.original.isEmpty
    }
    
    private func getElement() -> [(key: String, value: [String])] {
        return self.original
    }
    
    private func getValue(indexPath: IndexPath) -> String {
        return self.original[indexPath.section].value[indexPath.row]
    }
    
    public func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 60.0
    }
    
    public func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.0
    }
}
