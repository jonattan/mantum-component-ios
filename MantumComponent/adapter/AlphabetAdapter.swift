//
//  AlphabetAdapter.swift
//  MantumComponent
//
//  Created by Jonattan Velásquez on 24/05/18.
//  Copyright © 2018 Mantum. All rights reserved.
//

public class AlphabetAdapter<V: SimpleViewHandler, K: UITableViewCell>: TableViewAdapter<V, K>, UITableViewDataSource, UITableViewDelegate {
    
    private var original = [(key: String, value: [V])]()
    
    public func includeNewElements(value: [V], sort: Bool = true) -> Void {
        self.original.removeAll()
        
        value.forEach { item in
            let title = item.title()
            if title.isEmpty {
                return
            }
            
            let letter = self.checkLetter(value: title)
            let index = self.original.index(where: { (key, value) -> Bool in
                return key == letter
            })
            
            if index == nil {
                self.original.append((key: letter, value: [item]))
            } else {
                var include: Bool = true
                self.original[index!].value.forEach({ original in
                    if original.isEqual(value: item) {
                        include = false
                        return
                    }
                })
                
                if include {
                    self.original[index!].value.append(item)
                }
            }
        }
        
        if sort {
            self.original = self.original.sorted(by: { before, after in
                before.key.localizedCaseInsensitiveCompare(after.key) == .orderedAscending
            })
        }
    }
    
    public func sectionIndexTitles(for tableView: UITableView) -> [String]? {
        return self.original.map { (arg) -> String in
            let (key, _) = arg
            return key
        }
    }
    
    public func numberOfSections(in tableView: UITableView) -> Int {
        return self.original.count
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.original[section].value.count
    }
    
    public func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return original[section].key.uppercased()
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return self.onCellForRowAt(self.getValue(indexPath: indexPath), indexPath, tableView)
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.onSelected != nil {
            self.onSelected!(getValue(indexPath: indexPath))
        }
    }
    
    public func isEmpty() -> Bool {
        return self.original.isEmpty
    }
    
    private func getElement() -> [(key: String, value: [V])] {
        return self.original
    }
    
    private func getValue(indexPath: IndexPath) -> V {
        return self.original[indexPath.section].value[indexPath.row]
    }
}
