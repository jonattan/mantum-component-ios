//
//  GalleryPreviewAdapter.swift
//  MantumComponent
//
//  Created by Jonattan Velásquez on 21/06/18.
//  Copyright © 2018 Mantum. All rights reserved.
//

import Foundation

public class GalleryPreviewAdapter<V: GalleryViewHandler, K: UICollectionViewCell>: NSObject, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    private var original: [V] = []
    
    public var passedContentOffset = IndexPath()
    
    let onCellForRowAt: ((_ value: V, _ indexPath: IndexPath, _ collectionView: UICollectionView) -> K)
    
    public init(onCellForRowAt: @escaping (V, IndexPath, UICollectionView) -> K) {
        self.onCellForRowAt = onCellForRowAt
        super.init()
    }
    
    public func includeNewElements(value: [V]) -> Void {
        self.original.removeAll()
        value.forEach { (item) in
            var include = true
            self.original.forEach({ (original) in
                if original.isEqual(value: item) {
                    include = false
                    return
                }
            })
            
            if include {
                self.original.append(item)
            }
        }
    }
    
    public func build(collectionView: UICollectionView) -> Void {
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(K.self, forCellWithReuseIdentifier: "Cell")
        collectionView.isPagingEnabled = true
        collectionView.autoresizingMask = UIView.AutoresizingMask(rawValue: UIView.AutoresizingMask.RawValue(UInt8(UIView.AutoresizingMask.flexibleWidth.rawValue) | UInt8(UIView.AutoresizingMask.flexibleHeight.rawValue)))
    }
    
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.original.count
    }
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        return self.onCellForRowAt(self.getValue(indexPath: indexPath), indexPath, collectionView)
    }
    
    public func viewWillLayoutSubviews(collectionView: UICollectionView) -> Void {
        guard let flowLayout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout else {
            return
        }
        
        flowLayout.itemSize = collectionView.frame.size
        flowLayout.invalidateLayout()
        collectionView.collectionViewLayout.invalidateLayout()
    }
    
    public func viewWillTransition(collectionView: UICollectionView, size: CGSize, coordinator: UIViewControllerTransitionCoordinator) -> Void {
        let offset = collectionView.contentOffset
        let width  = collectionView.bounds.size.width
        
        let index = round(offset.x / width)
        let newOffset = CGPoint(x: index * size.width, y: offset.y)
        
        collectionView.setContentOffset(newOffset, animated: false)
        coordinator.animate(alongsideTransition: { (context) in
            collectionView.reloadData()
            collectionView.setContentOffset(newOffset, animated: false)
        }, completion: nil)
    }

    private func getValue(indexPath: IndexPath) -> V {
        return self.original[indexPath.row]
    }
    
}

open class ImagePreviewFullViewCell: UICollectionViewCell, UIScrollViewDelegate {
    
    private var scrollImg: UIScrollView!
    private var imgView: UIImageView!
    
    public func agregar(url: String) -> Void {
        let image = Image(url: url)
        image.download { (image, error) in
            DispatchQueue.main.async {
                self.imgView.image = image
            }
        }
    }
    
    override public init(frame: CGRect) {
        super.init(frame: frame)
        
        scrollImg = UIScrollView()
        scrollImg.delegate = self
        scrollImg.alwaysBounceVertical = false
        scrollImg.alwaysBounceHorizontal = false
        scrollImg.showsVerticalScrollIndicator = true
        scrollImg.flashScrollIndicators()
        
        scrollImg.minimumZoomScale = 1.0
        scrollImg.maximumZoomScale = 4.0
        
        let doubleTapGest = UITapGestureRecognizer(target: self, action: #selector(handleDoubleTapScrollView(recognizer:)))
        doubleTapGest.numberOfTapsRequired = 2
        scrollImg.addGestureRecognizer(doubleTapGest)
        
        self.addSubview(scrollImg)
        
        imgView = UIImageView()
        imgView.image = UIImage(named: "user3")
        scrollImg.addSubview(imgView!)
        imgView.contentMode = .scaleAspectFit
    }
    
    @objc private func handleDoubleTapScrollView(recognizer: UITapGestureRecognizer) {
        if scrollImg.zoomScale == 1 {
            scrollImg.zoom(to: zoomRectForScale(scale: scrollImg.maximumZoomScale, center: recognizer.location(in: recognizer.view)), animated: true)
        } else {
            scrollImg.setZoomScale(1, animated: true)
        }
    }
    
    private func zoomRectForScale(scale: CGFloat, center: CGPoint) -> CGRect {
        var zoomRect = CGRect.zero
        zoomRect.size.height = imgView.frame.size.height / scale
        zoomRect.size.width  = imgView.frame.size.width  / scale
        
        let newCenter = imgView.convert(center, from: scrollImg)
        zoomRect.origin.x = newCenter.x - (zoomRect.size.width / 2.0)
        zoomRect.origin.y = newCenter.y - (zoomRect.size.height / 2.0)
        return zoomRect
    }
    
    public func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return self.imgView
    }
    
    override open func layoutSubviews() {
        super.layoutSubviews()
        scrollImg.frame = self.bounds
        imgView.frame = self.bounds
    }
    
    override open func prepareForReuse() {
        super.prepareForReuse()
        scrollImg.setZoomScale(1, animated: true)
    }
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
