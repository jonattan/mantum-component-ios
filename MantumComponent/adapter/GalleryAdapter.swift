//
//  GaleryAdapter.swift
//  MantumComponent
//
//  Created by Jonattan Velásquez on 20/06/18.
//  Copyright © 2018 Mantum. All rights reserved.
//

import UIKit

public class GalleryAdapter<V: GalleryViewHandler, K: UICollectionViewCell>: NSObject, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    private var original: [V] = []
    private var width: CGFloat = 0

    public var onSelected: ((V, IndexPath) -> Void)?
    public var onCellForRowAt: ((_ value: V, _ indexPath: IndexPath, _ collectionView: UICollectionView) -> K)?
    
    public func includeNewElements(value: [V]) -> Void {
        self.original.removeAll()
        
        value.forEach { (item) in            
            var include = true
            var index = 0
            self.original.forEach({ (original) in
                if original.isEqual(value: item) {
                    include = false
                    return
                }
                index = index + 1
            })
            
            if include {
                self.original.append(item)
            }
        }
    }
 
    public func frameWidth(collectionView: UICollectionView) -> Void {
        self.width = collectionView.frame.width
    }
    
    public func autoresizingMask() -> UIView.AutoresizingMask {
        return UIView.AutoresizingMask(rawValue: UIView.AutoresizingMask.RawValue(UInt8(UIView.AutoresizingMask.flexibleWidth.rawValue) | UInt8(UIView.AutoresizingMask.flexibleHeight.rawValue)))
    }

    
    public func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if onSelected != nil {
            onSelected!(self.original[indexPath.row], indexPath)
        }
    }
    
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.original.count
    }
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        return self.onCellForRowAt!(self.getValue(indexPath: indexPath), indexPath, collectionView)
    }
    
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.width/2 - 5, height: self.width/2 + 35)
    }
    
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10.0
    }
    
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 5.0
    }
    
    private func getValue(indexPath: IndexPath) -> V {
        return self.original[indexPath.row]
    }
    
    public func getOriginal() -> [V] {
        return self.original
    }
    
    public func isEmpty() -> Bool {
        return self.original.isEmpty
    }
}
