//
//  SimpleAdapter.swift
//  MantumComponent
//
//  Created by Jonattan Velásquez on 20/06/18.
//  Copyright © 2018 Mantum. All rights reserved.
//

import Foundation

public class SimpleAdapter<V: SimpleViewHandler, K: UITableViewCell>: TableViewAdapter<V, K>, UITableViewDataSource, UITableViewDelegate {
    
    public var footerColor: String = "FFFFFF"
    
    private var original: [V] = []
    
    public func includeNewElements(value: [V]) -> Void {
        self.original.removeAll()
        
        value.forEach { (item) in
            let title = item.title()
            if title.isEmpty {
                return
            }
            
            var include = true
            self.original.forEach({ (original) in
                if original.isEqual(value: item) {
                    include = false
                    return
                }
            })
            
            if include {
                self.original.append(item)
            }
        }
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.original.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return self.onCellForRowAt(self.getValue(indexPath: indexPath), indexPath, tableView)
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.onSelected != nil {
            self.onSelected!(getValue(indexPath: indexPath))
        }
    }
    
    public func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 1))
        footerView.backgroundColor = MantumController.hexStringToUIColor(hex: footerColor)
        return footerView
    }
    
    private func getValue(indexPath: IndexPath) -> V {
        return self.original[indexPath.row]
    }
    
    public func isEmpty() -> Bool {
        return self.original.isEmpty
    }
}
