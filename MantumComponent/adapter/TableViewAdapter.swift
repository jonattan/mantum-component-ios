//
//  Adapter.swift
//  MantumComponent
//
//  Created by Jonattan Velásquez on 20/06/18.
//  Copyright © 2018 Mantum. All rights reserved.
//

import Foundation

open class TableViewAdapter<V: SimpleViewHandler, K: UITableViewCell>: NSObject {
    
    let characters: String = "abcdefghijklmnñopqrstuvwxyzABCDEFGHIJKLMNÑOPQRSTUVWXYZ0123456789"
    
    public var onSelected: ((V) -> Void)?
    
    public var onCellForRowAt: ((_ value: V, _ indexPath: IndexPath, _ tableView: UITableView) -> K)!
    
    func checkLetter(value: String) -> String {
        let normalize = value.folding(options: .diacriticInsensitive, locale: .current)
        let letter = String(normalize[...normalize.index(normalize.startIndex, offsetBy: 0)])
        
        let characterset = CharacterSet(charactersIn: self.characters)
        if letter.rangeOfCharacter(from: characterset.inverted) != nil {
            let startIndex = value.index(value.startIndex, offsetBy: 1)
            return checkLetter(value: String(value[startIndex...]))
        }
        return letter.uppercased()
    }
}
