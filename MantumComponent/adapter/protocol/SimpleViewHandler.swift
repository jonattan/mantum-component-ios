//
//  Alphabet.swift
//  MantumComponent
//
//  Created by Jonattan Velásquez on 24/05/18.
//  Copyright © 2018 Mantum. All rights reserved.
//

public protocol SimpleViewHandler {
    
    func title() -> String
    
    func isEqual<T: Any>(value: T) -> Bool
}
