//
//  GalleryViewHandler.swift
//  MantumComponent
//
//  Created by Jonattan Velásquez on 20/06/18.
//  Copyright © 2018 Mantum. All rights reserved.
//

import Foundation

public protocol GalleryViewHandler {
    
    func isEqual<T: Any>(value: T) -> Bool
}
