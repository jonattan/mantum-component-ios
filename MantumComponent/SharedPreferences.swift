//
//  SharedPreferences.swift
//  MantumComponent
//
//  Created by Mántum on 10/24/18.
//  Copyright © 2018 Mantum. All rights reserved.
//

import Foundation

public class SharedPreferences {
    
    public static func getStringValue(id: String) -> String {
        let preferences = UserDefaults.standard
        let currentLevelKey = id
        var currentLevel = ""
        if preferences.object(forKey: currentLevelKey) != nil {
            currentLevel = preferences.string(forKey: currentLevelKey)!
        }
        return currentLevel
    }
    
    public static func setStringValue(id: String, value: String) {
        let preferences = UserDefaults.standard
        let currentLevel = value
        let currentLevelKey = id
        preferences.set(currentLevel, forKey: currentLevelKey)
        preferences.synchronize()
    }
    
    public static func getIntValue(id: String) -> Int64 {
        let preferences = UserDefaults.standard
        let currentLevelKey = id
        var currentLevel = 0
        if preferences.object(forKey: currentLevelKey) != nil {
            currentLevel = preferences.integer(forKey: currentLevelKey)
        }
        return Int64(currentLevel)
    }
    
    public static func setIntValue(id: String, value: Int64) {
        let preferences = UserDefaults.standard
        let currentLevel = value
        let currentLevelKey = id
        preferences.set(currentLevel, forKey: currentLevelKey)
        preferences.synchronize()
    }
    
}
