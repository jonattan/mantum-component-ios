//
//  Image.swift
//  MantumComponent
//
//  Created by Jonattan Velásquez on 21/06/18.
//  Copyright © 2018 Mantum. All rights reserved.
//

import Foundation

public class Image {
    
    private static let imageCache = NSCache<NSString, UIImage>()
    
    private let url: URL
    private let persist: Bool
    
    public init(url: URL, persist: Bool = true) {
        self.url = url
        self.persist = persist
    }
    
    public init(url: String, persist: Bool = true) {
        let value = url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        self.url = URL(string: value!)!
        self.persist = persist
    }
    
    public func download(completion: @escaping (UIImage?, Error?) -> ()) -> Void {
        if let cachedImage = Image.imageCache.object(forKey: url.absoluteString as NSString) {
            completion(cachedImage, nil)
            return
        }
        
        let local = filePath(name: self.url.lastPathComponent)
        if let result = try? local?.checkResourceIsReachable(), result! {
            let image = UIImage(contentsOfFile: (local?.path)!)
            Image.imageCache.setObject(image!, forKey: self.url.absoluteString as NSString)
            completion(image, nil)
            return
        }
        
        if !Services.isConnectedToNetwork() {
            NSLog("No tiene una conexión estable a internet")
            return
        }
        
        var urlRequest = URLRequest(url : url)
        urlRequest.httpMethod = "GET"
        urlRequest.timeoutInterval = 300
        
        print("\(Date()) HTTP GET -> \(url)")
        let task = URLSession.shared.dataTask(with: urlRequest) { data, response, error in
            print("\(Date()) HTTP GET <- \(self.url)")
            guard let data = data, error == nil else {
                NSLog("Ocurrio un error al descargar la imagen (\(self.url.absoluteString)")
                return
            }
            
            if let image = UIImage(data: data) {
                if let url = self.filePath(name: self.url.lastPathComponent) {
                    self.write(image: image, filePath: url)
                }
                completion(image, error)
            } else {
                NSLog("Ocurrio un error al agregar la imagen a la memoria cache \(self.url.absoluteString)")
            }
        }
        
        task.resume()
    }
    
    private func filePath(name: String) -> URL? {
        let documentDirectory = FileManager.SearchPathDirectory.documentDirectory
        let userDomainMask = FileManager.SearchPathDomainMask.userDomainMask
        let searchPathForDirectoriesInDomains = NSSearchPathForDirectoriesInDomains(documentDirectory, userDomainMask, true)
        if let filePath = searchPathForDirectoriesInDomains.first {
            return URL(fileURLWithPath: filePath).appendingPathComponent(name)
        }
        return nil
    }
    
    private func write(image: UIImage, filePath: URL) -> Void {
        Image.imageCache.setObject(image, forKey: self.url.absoluteString as NSString)
        if !self.persist {
            return
        }
        
        switch filePath.pathExtension {
        case "png":
            if (try? image.pngData()?.write(to: filePath)) != nil {
                NSLog("Save PNG", filePath.lastPathComponent)
            }
            break
        case "jpg":
            if (try? image.jpegData(compressionQuality: 100)?.write(to: filePath)) != nil {
                NSLog("Save JPG", filePath.lastPathComponent)
            }
            break
        default: break
        }

    }
}
