//
//  Location.swift
//  MantumComponent
//
//  Created by Jonattan Velasquez on 11/12/18.
//  Copyright © 2018 Mantum. All rights reserved.
//

import CoreLocation

public class Location: NSObject, CLLocationManagerDelegate {
 
    private var locationManager: CLLocationManager?
    
    public var onLocation: ((_ locations: [CLLocation]) -> Void)?
    
    public override init() {
        super.init()
        
        // https://stackoverflow.com/questions/5930612/how-to-set-accuracy-and-distance-filter-when-using-mkmapview
        self.locationManager = CLLocationManager()
        self.locationManager?.delegate = self
        self.locationManager?.distanceFilter = 10
        self.locationManager?.headingFilter = 5
        self.locationManager?.activityType = CLActivityType.automotiveNavigation
        self.locationManager?.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
        self.locationManager?.allowsBackgroundLocationUpdates = true
        self.locationManager?.pausesLocationUpdatesAutomatically = false
        self.locationManager?.requestAlwaysAuthorization()
    }
    
    public func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedAlways {
            self.locationManager?.startUpdatingLocation()
        }
    }
    
    public func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if onLocation != nil {
            self.onLocation!(locations)
        }
    }
    
    public func start() -> Void {
        if self.locationManager != nil {
            self.locationManager?.startUpdatingLocation()
        }
    }
    
    public func stop() -> Void {
        if self.locationManager != nil {
            self.locationManager?.stopUpdatingLocation()
        }
    }
}
