//
//  Json.swift
//  MantumComponent
//
//  Created by Jonattan Velasquez on 11/7/18.
//  Copyright © 2018 Mantum. All rights reserved.
//
import Foundation

public class Json {
    
    private var data: [String : Any]
    
    public init(data: [String : Any]) {
        self.data = data
    }
    
    public init(data: [String: Any], key: String) {
        self.data = data[key] as! [String : Any]
    }
    
    public func asBody() -> Json {
        if self.data["body"] is [String: Any] {
            self.data = self.data["body"] as? [String: Any] ?? [:]
        }
        return self
    }
    
    public func asString(key: String) -> String? {
        if self.data[key] is String {
            return self.data[key] as? String
        }
        return nil
    }
    
    public func asString(key: String, defaultValue: String) -> String {
        if self.data[key] is String {
            return self.data[key] as? String ?? defaultValue
        }
        return defaultValue
    }
    
    public func asInt(key: String, defaultValue: Int) -> Int {
        if self.data[key] is Int {
            return self.data[key] as? Int ?? defaultValue
        }
        
        if self.data[key] is String {
            if let value = self.data[key] as? String {
                return Int(value) ?? defaultValue
            }
        }
        
        return defaultValue
    }
    
    public func asArrayInt64(key: String) -> [Int64] {
        if self.data[key] is [Int64] {
            return self.data[key] as? [Int64] ?? []
        }
        return []
    }
    
    public func asInt64(key: String) -> Int64? {
        if self.data[key] is Int64 {
            return self.data[key] as? Int64
        }
        
        if self.data[key] is String {
            if let value = self.data[key] as? String {
                return Int64(value)
            }
        }
        
        return nil
    }
    
    public func asDate(key: String, dateFormat: String) -> Date? {
        if self.data[key] is String {
            if let value = self.data[key] as? String, !value.isEmpty, value != "Ninguna" {
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = dateFormat
                return dateFormatter.date(from: value)
            }
        }
        return nil
    }
    
    public func asInt64(key: String, defaultValue: Int64) -> Int64 {
        if self.data[key] is Int64 {
            return self.data[key] as? Int64 ?? defaultValue
        }
        
        if self.data[key] is String {
            if let value = self.data[key] as? String {
                return Int64(value) ?? defaultValue
            }
        }
        
        return defaultValue
    }
    
    public func asArray(key: String) -> [[String: Any]] {
        if self.data[key] is [[String: Any]] {
            if let value = self.data[key] as? [[String: Any]] {
                return value
            }
        }
        return []
    }
    
    public func asDouble(key: String, defaultValue: Double) -> Double {
        if self.data[key] is Int {
            return self.data[key] as? Double ?? defaultValue
        }
        
        if self.data[key] is String {
            if let value = self.data[key] as? String {
                return Double(value) ?? defaultValue
            }
        }
        
        return defaultValue
    }
    
    public func asBool(key: String) -> Bool {
        if self.data[key] is Bool {
            return self.data[key] as? Bool ?? false
        }
        
        if self.data[key] is String {
            if let value = self.data[key] as? String {
                return value == "true" || value == "t" || value == "1"
            }
        }
        
        return false
    }
}
